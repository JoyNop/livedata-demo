const Gulp = require('gulp')
const Typescript = require('gulp-typescript')
const Sourcemaps = require('gulp-sourcemaps')
const Stylus = require('gulp-stylus')
const Webpack = require('webpack-stream')
const TerserPlugin = require('terser-webpack-plugin')
const Rimraf = require('rimraf')
const Path = require('path')

const PROJECT_DIR = Path.resolve(__dirname, '..')
const SOURCE_DIR = Path.join(PROJECT_DIR, 'src')
const SOURCE_BROWSER_DIR = Path.join(SOURCE_DIR, 'browser')
const DIST_DIR = Path.join(PROJECT_DIR, 'dist')
const DIST_BROWSER_CODE_DIR = Path.join(DIST_DIR, 'browser/code')

var debug = false

function doubleChars(x) {
  return String(x).length < 2 ? '0' + String(x) : String(x)
}

function withWatchChange(watcher) {
  watcher.on('change', function(path, stats) {
    const now = new Date()
    const time =
      doubleChars(now.getHours()) +
      ':' +
      doubleChars(now.getMinutes()) +
      ':' +
      doubleChars(now.getSeconds())
    const pathname = Path.relative(process.cwd(), path)
    console.log(
      '[\033[90m' + time + '\033[0m] ' + pathname + ' \033[35mchanged\033[0m'
    )
  })
}

Gulp.task('clear-dist', function(cb) {
  Rimraf.sync(DIST_DIR)
  cb()
})

Gulp.task('build-browser', function() {
  const options = {
    target: 'web',
    mode: 'development',

    entry: {
      main: Path.join(SOURCE_BROWSER_DIR, 'main.tsx'),
    },

    output: {
      filename: '[name].js',
    },

    externals: {
      react: 'React',
      'react-dom': 'ReactDOM',
      'react-router-dom': 'ReactRouterDOM',

      // '@material-ui/core': 'window["material-ui"]',
      // '@material-ui/styles': 'window["material-ui"]'
    },

    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      alias: {
        browser: SOURCE_BROWSER_DIR,
      },
    },

    devtool: 'source-map',

    module: {
      rules: [
        {
          test: /\.tsx$/,
          use: ['awesome-typescript-loader'],
          exclude: /(node_modules)|(src\/server)/,
        },
        // {
        //   test: /\.styl$/,
        //   use: [
        //     {
        //       loader: 'style-loader'
        //     },
        //     {
        //       loader: 'css-loader',
        //       options: {
        //         modules: true
        //       }
        //     },
        //     {
        //       loader: 'stylus-loader'
        //     }
        //   ],
        //   exclude: /(node_modules)|(src\/server)/
        // }
      ],
    },

    plugins: [],
    watch: true,
    watchOptions: {
      ignored: [/node_modules/, /src\/server/],
    },
  }
  if (!debug) {
    options.mode = 'production'
    delete options.devtool
    delete options.watch
    options.plugins.push(new TerserPlugin())
  }
  // const webpackStream = Webpack(options).on('error', function (error) {
  //   console.error(error.toString())
  //   this.emit('end')
  // })
  return Gulp.src(SOURCE_BROWSER_DIR, {
    base: SOURCE_BROWSER_DIR,
  })
    .pipe(Webpack(options))
    .pipe(Gulp.dest(DIST_BROWSER_CODE_DIR))
})

Gulp.task('debug', function(cb) {
  debug = true
  cb()
})

Gulp.task('build', Gulp.series('clear-dist', 'build-browser'))
Gulp.task('watch', Gulp.series('debug', 'clear-dist', 'build-browser'))
