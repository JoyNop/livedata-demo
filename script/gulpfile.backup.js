const Gulp = require('gulp')
const Typescript = require('gulp-typescript')
const Sourcemaps = require('gulp-sourcemaps')
const Stylus = require('gulp-stylus')
const Webpack = require('webpack-stream')
const TerserPlugin = require('terser-webpack-plugin')
const Rimraf = require('rimraf')
const Path = require('path')
const Fs = require('fs')
const tsconfig = require('./tsconfig.json')

const SOURCE_DIR = Path.join(__dirname, 'src')
const SOURCE_CLIENT_DIR = Path.join(SOURCE_DIR, 'client')
const SOURCE_CLIENT_STYLE = Path.join(SOURCE_CLIENT_DIR, './client.styl')
const DIST_DIR = Path.join(__dirname, 'dist')
const DIST_CLIENT_CODE_DIR = Path.join(DIST_DIR, 'client/code')
const NODE_MODULES_DIR = Path.join(__dirname, 'node_modules')
const PUBLIC_CODE_DIR = Path.join(__dirname, 'public/code')
const PUBLIC_LIB_DIR = Path.join(__dirname, 'public/lib')

var debug = false

function doubleChars(x) {
  return String(x).length < 2 ? '0' + String(x) : String(x)
}

function withWatchChange(watcher) {
  watcher.on('change', function(path, stats) {
    const now = new Date()
    const time =
      doubleChars(now.getHours()) +
      ':' +
      doubleChars(now.getMinutes()) +
      ':' +
      doubleChars(now.getSeconds())
    const pathname = Path.relative(process.cwd(), path)
    console.log(
      '[\033[90m' + time + '\033[0m] ' + pathname + ' \033[35mchanged\033[0m'
    )
  })
}

Gulp.task('clear-dist', function(cb) {
  Rimraf.sync(DIST_DIR)
  cb()
})

// Gulp.task('copy', function () {

//   return Gulp
//            .src(Path.join(SOURCE_DIR, './**/*.!(styl|ts|tsx|*.styl|*.tsx|*.ts)'), {
//              base: SOURCE_DIR,
//            })
//            .pipe(Gulp.dest(DIST_DIR))
// })

Gulp.task('build-client-js', function() {
  const options = {
    target: 'web',
    mode: 'development',

    entry: {
      client: './src/client/client.tsx',
    },

    output: {
      filename: './[name].js',
    },

    externals: {
      react: 'React',
      'react-dom': 'ReactDOM',
      // '@material-ui/core': 'window["material-ui"]',
      // '@material-ui/styles': 'window["material-ui"]'
    },

    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
      alias: {
        client: SOURCE_CLIENT_DIR,
      },
    },

    devtool: 'source-map',

    module: {
      rules: [
        {
          test: /\.tsx$/,
          use: ['awesome-typescript-loader'],
          exclude: /(node_modules)|(src\/server)/,
        },
        {
          test: /\.styl$/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: {
                modules: true,
              },
            },
            {
              loader: 'stylus-loader',
            },
          ],
          exclude: /(node_modules)|(src\/server)/,
        },
      ],
    },

    plugins: [],
    watch: true,
    watchOptions: {
      ignored: [/node_modules/, /src\/server/],
    },
  }
  if (!debug) {
    options.mode = 'production'
    delete options.devtool
    delete options.watch
    options.plugins.push(new TerserPlugin())
  }
  // const webpackStream = Webpack(options).on('error', function (error) {
  //   console.error(error.toString())
  //   this.emit('end')
  // })
  return Gulp.src(SOURCE_CLIENT_DIR, {
    base: SOURCE_CLIENT_DIR,
  })
    .pipe(Webpack(options))
    .pipe(Gulp.dest(DIST_CLIENT_CODE_DIR))
})

Gulp.task('build-client-css', function() {
  // var ss = Stylus().on('error', function (error) {
  //   console.error(error.toString())
  //   this.emit('end')
  // })
  var entries = []
  var dirs = [SOURCE_CLIENT_DIR]
  for (let dir of dirs) {
    let names = Fs.readdirSync(dir)
    for (let name of names) {
      let filename = Path.join(dir, name)
      let stats = Fs.statSync(filename)
      if (
        stats.isFile() &&
        Path.extname(filename) === '.styl' &&
        filename !== SOURCE_CLIENT_STYLE
      ) {
        entries.push(filename)
      } else if (stats.isDirectory()) {
        dirs.push(filename)
      }
    }
  }
  Fs.writeFileSync(
    SOURCE_CLIENT_STYLE,
    entries.map(x => `@import "${x}"`).join('\n')
  )
  var stream = Gulp.src([SOURCE_CLIENT_STYLE], {
    base: SOURCE_CLIENT_DIR,
  })
  if (debug) {
    stream = stream.pipe(Sourcemaps.init())
  }
  stream = stream.pipe(Stylus())
  if (debug) {
    stream = stream.pipe(Sourcemaps.write('.'))
  }
  stream = stream.pipe(Gulp.dest(DIST_CLIENT_CODE_DIR))
  return stream
})

Gulp.task('watch-client-css', function() {
  withWatchChange(
    Gulp.watch(
      ['src/client/**/*.styl', '!src/client/client.styl'],
      Gulp.series('build-client-css')
    )
  )
})

Gulp.task('debug', function(cb) {
  debug = true
  cb()
})

Gulp.task(
  'build',
  Gulp.series(
    'clear-dist',
    Gulp.parallel('build-client-css', 'build-client-js')
  )
)
Gulp.task(
  'watch',
  Gulp.series(
    'debug',
    'clear-dist',
    Gulp.parallel(
      Gulp.series('build-client-css', 'watch-client-css'),
      'build-client-js'
    )
  )
)

Gulp.task('clear-public-lib-and-code', function(cb) {
  Rimraf.sync(PUBLIC_LIB_DIR)
  cb()
})
Gulp.task('copy-code', function() {
  return Gulp.src(Path.join(DIST_DIR, './client/code/**/**'), {
    base: Path.join(DIST_DIR, './client/code'),
  }).pipe(Gulp.dest(PUBLIC_CODE_DIR))
})
Gulp.task('copy-react', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './react/umd/react.production.min.js'),
    {
      base: Path.join(NODE_MODULES_DIR, './react/umd'),
    }
  ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'react')))
})
Gulp.task('copy-react-dom', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './react-dom/umd/react-dom.production.min.js'),
    {
      base: Path.join(NODE_MODULES_DIR, './react-dom/umd'),
    }
  ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'react')))
})
Gulp.task('copy-material-ui', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './@material-ui/core/umd/*.@(min.js|min.css)'),
    {
      base: Path.join(NODE_MODULES_DIR, './@material-ui/core/umd'),
    }
  ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'material-ui')))
})
Gulp.task('copy-material-design-icons', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './material-design-icons/**/**'),
    {
      base: NODE_MODULES_DIR,
    }
  ).pipe(Gulp.dest(PUBLIC_LIB_DIR))
})
Gulp.task(
  'copy',
  Gulp.series(
    'clear-public-lib-and-code',
    Gulp.parallel(
      'copy-code',
      'copy-react',
      'copy-react-dom',
      'copy-material-ui',
      'copy-material-design-icons'
    )
  )
)
