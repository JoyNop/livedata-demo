const Gulp = require('gulp')
const Rimraf = require('rimraf')
const Path = require('path')

const PROJECT_DIR = Path.resolve(__dirname, '..')
const DIST_DIR = Path.join(PROJECT_DIR, 'dist')
const NODE_MODULES_DIR = Path.join(PROJECT_DIR, 'node_modules')
const PUBLIC_CODE_DIR = Path.join(PROJECT_DIR, 'public/code')
const PUBLIC_LIB_DIR = Path.join(PROJECT_DIR, 'public/lib')

Gulp.task('clear-public-lib-and-code', function(cb) {
  Rimraf.sync(PUBLIC_LIB_DIR)
  cb()
})

Gulp.task('copy-code', function() {
  return Gulp.src(Path.join(DIST_DIR, './client/code/**/**'), {
    base: Path.join(DIST_DIR, './client/code'),
  }).pipe(Gulp.dest(PUBLIC_CODE_DIR))
})

Gulp.task('copy-react', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './react/umd/react.production.min.js'),
    {
      base: Path.join(NODE_MODULES_DIR, './react/umd'),
    }
  ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'react')))
})

Gulp.task('copy-react-dom', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './react-dom/umd/react-dom.production.min.js'),
    {
      base: Path.join(NODE_MODULES_DIR, './react-dom/umd'),
    }
  ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'react')))
})

Gulp.task('copy-react-router-dom', function() {
  return Gulp.src(
    Path.join(
      NODE_MODULES_DIR,
      './react-router-dom/umd/react-router-dom.min.js'
    ),
    {
      base: Path.join(NODE_MODULES_DIR, './react-router-dom/umd'),
    }
  ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'react')))
})

Gulp.task('copy-material-ui', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './@material-ui/core/umd/*.@(min.js|min.css)'),
    {
      base: Path.join(NODE_MODULES_DIR, './@material-ui/core/umd'),
    }
  ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'material-ui')))
})

Gulp.task('copy-material-design-icons', function() {
  return Gulp.src(
    Path.join(NODE_MODULES_DIR, './material-design-icons/**/**'),
    {
      base: NODE_MODULES_DIR,
    }
  ).pipe(Gulp.dest(PUBLIC_LIB_DIR))
})

// Gulp.task('copy-React-Toastify', function() {
//   return Gulp.src(
//     Path.join(NODE_MODULES_DIR, './react-toastify/dist/ReactToastify.min.css'),
//     {
//       base: Path.join(NODE_MODULES_DIR, './react-toastify/dist'),
//     }
//   ).pipe(Gulp.dest(Path.join(PUBLIC_LIB_DIR, 'react-toastify')))
// })

Gulp.task(
  'copy',
  Gulp.series(
    'clear-public-lib-and-code',
    Gulp.parallel(
      'copy-code',
      'copy-react',
      'copy-react-dom',
      'copy-react-router-dom',
      'copy-material-ui',
      'copy-material-design-icons',
      'copy-React-Toastify'
    )
  )
)
