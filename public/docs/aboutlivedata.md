By WangJunhua
<!--more-->

# 新框架的应用分享

新框架使用liveData来进行组件间通信和前后端交互

![image](https://duobanghuide-builder.oss-cn-beijing.aliyuncs.com/testoss/user/6/前端框架.jpeg)

## 在Component1中实例化LiveData和Service，传入到需要的子组件中

```
private live = new ComponentLiveData()
private service = new ComponentService()

<Component2 live={this.live} service={this.service} />
<Component3 live={this.live} service={this.service} />
```

## 组件间通信

### 定义一个组件通信的LiveData类

```
export interface ComponentId extends Component2Id {}

export class ComponentLiveData extends LivaData<ComponentId> implements Component2LiveData {
    private originValue: ComponentId = Object.create(null)
    
    public setListId(listId: number) {
        this.originValue.listId = listId
        this.set(this.originValue)
    }
}
```

点击Component2中的列表，将列表数据传到Component3

```
// 在组件Component2中
export interface Component2Id {
    listId: number
}

export interface Component2LiveData extends LivaData<Component2Id> {
    setListId: (listId: number): void
}

```
在获取到列表时，将第一项的id传入到LiveData，保证Component3中最开始加载有数据

之后在点击列表时传入id值

```
this.props.live.setListId(id)
```
在Component3中获取到Component2中传过来的数据

```
// 获取listId，此处判断是否存在（因为Component2和Component3同时加载）
let listId: number
if (this.props.live.value !== undefined) {
    listId = this.props.live.value.listId
}
```

## 前后端交互

### 定义一个交互LiveData类

```
export interface IListData {
    
}

export enum ComponentServiceStatus {
    PENDING, OK, ERROR
}

export class ComponentService extends LiveData<ComponentServiceStatus> {
    // 类型或一系列枚举
    public async getListDetailData(listId: number) {
        if (this.value !== ComponentServiceStatus.PENDING) {
            let listData: Array<IListData> = []
            this.set(ComponentServiceStatus.PENDING)
            try {
                listData = await axios.get('/api/...')
                this.set(ComponentServiceStatus.OK)
                return listData
            } catch (e) {
                // 一些事件
                this.set(ComponentServiceStatus.ERROR)
            }
        }
    }
}
```

在Component3中拿到listId后，可以进行ajax

```
this.props.service.getListDetailData(this.props.live.value.listId)
```


在Component3中可以观察传入的状态，来判断是否正在加载中，来判断是否显示进度条

```
constructor(props: IComponent3Props) {
    super(props)
    this.props.service.observe(this, status => {
        switch (status) {
            case ComponentServiceStatus.PENDING:
                this.isProgress = true
                this.setState({})
            break
            default:
                thi.isProgress = false
                this.setState({})
            break
        }
    )
}
```
