FROM        node:8.11.3
LABEL       author="Wang Tong"
LABEL       duobang.io/name=pms-browser
LABEL       duobang.io/category=web
LABEL       duobang.io/component=ui-server

RUN         mkdir /root/pms-browser
COPY        . /root/pms-browser/

ENV         PMS_BROWSER_HOST 0.0.0.0
ENV         PMS_BROWSER_PORT 10002

EXPOSE      10002
CMD         ["/root/pms-browser/script/docker-entrypoint.sh"]
