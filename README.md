pms-browser
================

# 配置

可以将browser搭建到docker中，也可以修改修改配置，使其在docker外运行，这里对docker外配置做个说明。  
修改configs/development.json配置：

```json
"browser": {
    "useProxy": true,
    "staticUrl": "/public",
    "staticDir": "/root/pms-browser/public",
    "codeDir": "/root/pms-browser/dist/browser/code"
}
```

将其中的staticDir与code，修改为本地的public、code对应的目录即可，如：

```json
"browser": {
    "useProxy": true,
    "staticUrl": "/public",
    "staticDir": "/home/sun/workspace/pms/pms-browser/public",
    "codeDir": "/home/sun/workspace/pms/pms-browser/dist/browser/code"
}
```

# 运行

下载后，依次运行：

- 安装
  npm install

- copy
  npm run copy

- 构建
  npm run build

- 启动
  npm run start
