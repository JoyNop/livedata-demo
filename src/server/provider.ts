import {
  Scope,
  ApplicationContext,
  SessionContext,
  ProviderContainer,
} from '@webnode/cdi'
import { RedisManager } from './redis'
import { ServerConfig } from './config'
import { Logger } from './logger'
import { RequestLog } from './log'

ProviderContainer.instance.set(
  Scope.APPLICATION,
  'config',
  ServerConfig.getInstance
)

ProviderContainer.instance.set(
  Scope.APPLICATION,
  'logger',
  (context: ApplicationContext) => {
    let config = context.value.providerContainer.get('config') as ServerConfig
    return new Logger(config.logger)
  }
)

ProviderContainer.instance.set(
  Scope.REQUEST,
  'requestLog',
  () => new RequestLog()
)
