import { ApplicationScope, RequestScope } from '@webnode/cdi'
import {
  RequestLifecycle,
  RequestStart,
  RequestEnd,
  RequestError,
} from '@webnode/http'
import { ServerRequest, ServerResponse, Req, Res } from '@webnode/http'
import { Logger } from './logger'
import { RequestLog } from './log'
import { ServerConfig } from './config'
import Chalk from 'chalk'

@RequestLifecycle
class DefaultConnectionLifecycle {
  constructor(
    @ApplicationScope('logger') private logger: Logger,
    @ApplicationScope('config') private config: ServerConfig
  ) {
    this.logger.info(`${config.name} started on port: ${config.port} `)
  }

  @RequestStart
  private requestStart(
    @Req req: ServerRequest,
    @Res res: ServerResponse,
    @RequestScope('requestLog') log: RequestLog
  ) {
    log.start = new Date()
  }

  @RequestEnd
  private requestEnd(
    @Req req: ServerRequest,
    @Res res: ServerResponse,
    @RequestScope('requestLog') log: RequestLog
  ) {
    log.end = new Date()

    let text: string
    if (this.config.logger.mode === 'console') {
      text = [
        `${req.remoteAddress}:${req.remotePort}`,
        Chalk.hex('#BF8F6F')(req.method),
        Chalk.hex('#0F8FBF')(req.url.path),
        Chalk.hex('#BF8F6F')(res.status.toString()),
        Chalk.green(`${log.end.getTime() - log.start.getTime()}ms`),
      ].join(' ')
    } else {
      text = [
        `${req.remoteAddress}:${req.remotePort}`,
        req.method,
        req.url.path,
        res.status.toString(),
        `${log.end.getTime() - log.start.getTime()}ms`,
      ].join(' ')
    }

    if (res.status >= 400) {
      this.logger.error(text)
    } else {
      this.logger.info(text)
    }
  }

  @RequestError
  private requestError() {}
}
