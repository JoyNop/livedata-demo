import { ApplicationScope, Scope, ProviderContainer } from '@webnode/cdi'
import { HttpServer } from '@webnode/http'
import { ServerConfig } from './config'

async function main() {
  require('./provider')

  require('./lifecycle')

  if (ServerConfig.getInstance().browser.useProxy) {
    require('./static.controller')
  }
  require('./html.controller')

  let app = HttpServer.create(ServerConfig.getInstance())
  await app.serve()
}

main()
