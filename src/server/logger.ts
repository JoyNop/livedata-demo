import * as Winston from 'winston'
import Chalk from 'chalk'

export interface LoggerConfig {
  mode: 'none' | 'console' | 'file'
  fatal?: string
  error?: string
  warning?: string
  info?: string
  debug?: string
}

export class Logger {
  private static FORMAT = 'YYYY-MM-DD HH:mm:ss'
  private static LEVEL_LIST = {
    fatal: 1,
    error: 2,
    warning: 3,
    info: 4,
    debug: 5,
  }

  private logger: Winston.Logger

  constructor(private config?: LoggerConfig) {
    this.create()
    this.setTransports()
  }

  private create() {
    this.logger = Winston.createLogger({
      levels: Logger.LEVEL_LIST,
      format: Winston.format.combine(
        Winston.format.timestamp({
          format: Logger.FORMAT,
        }),
        Winston.format.printf(info => {
          let colorizeL = ''
          let colorizeR = ''
          let levelL = ''
          let levelR = ''
          let level = info.level
          if (this.config.mode === 'console') {
            switch (info.level) {
              case 'fatal':
                level = Chalk.white.bgGreen('FATA')
                break
              case 'error':
                level = Chalk.white.bgGreen('ERRO')
                break
              case 'warning':
                level = Chalk.white.bgGreen('WARN')
                break
              case 'info':
                level = Chalk.white.bgGreen('INFO')
                break
              case 'debug':
                level = Chalk.white.bgCyan('DEBG')
                break
            }
          }
          return `${info.timestamp} ${level} ${info.message}`
        })
      ),
      transports: [],
      silent: this.config.mode === 'none' ? true : false,
    })
  }

  private setTransports() {
    switch (this.config.mode) {
      case 'console':
        this.logger.add(new Winston.transports.Console())
        break
      case 'file':
        if (typeof this.config !== 'object' || this.config === null) {
          this.config = Object.create(null)
        }
        for (let item of [
          {
            filename: this.config.fatal,
            level: 'fatal',
          },
          {
            filename: this.config.error,
            level: 'error',
          },
          {
            filename: this.config.warning,
            level: 'warning',
          },
          {
            filename: this.config.info,
            level: 'info',
          },
          {
            filename: this.config.debug,
            level: 'debug',
          },
        ]) {
          if (typeof item.filename === 'string') {
            this.logger.add(new Winston.transports.File(item))
          }
        }
        break
      default:
        break
    }
  }

  public fatal(s: string) {
    this.logger.log('fatal', s)
  }

  public error(s: string) {
    this.logger.log('error', s)
  }

  public warning(s: string) {
    this.logger.log('warning', s)
  }

  public info(s: string) {
    this.logger.log('info', s)
  }

  public debug(s: string) {
    this.logger.log('debug', s)
  }
}
