import { Helmet } from 'react-helmet'

export function render(
  dom: string,
  staticUrl: string,
  codeUrl: string,
  debug: boolean = true
): string {
  // SEO - Dynamic Helmet
  let helmetData = Helmet.renderStatic()
  return `<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  ${helmetData.meta.toString()}

  <title>基建管理云平台</title>
  ${helmetData.title.toString()}
  <link type="text/css"  rel="stylesheet" href="${staticUrl}/lib/react-toastify/ReactToastify.min.css"

  <link type="image/png" rel="icon"       href="${staticUrl}/images/favicon.png" sizes="32x32">
  <link type="text/css"  rel="stylesheet" href="${staticUrl}/css/normalize.css">
  <link type="text/css"  rel="stylesheet" href="${staticUrl}/lib/material-design-icons/iconfont/material-icons.css">
  <style>
    #root-meta {
      position: fixed;
      z-index: 10000;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background: #f6f6f6;
    }
  </style>
</head>

<body>
  <div id="root-meta" data-debug="${debug}"></div>
  <div id="root">${dom}</div>

  <script src="${staticUrl}/lib/react/react.production.min.js"></script>
  <script src="${staticUrl}/lib/react/react-dom.production.min.js"></script>
  <script src="${staticUrl}/lib/react/react-router-dom.min.js"></script>  
  <script src="${codeUrl}/code/main.js"></script>

  <script>
  ;(function () {
    document.body.removeChild(document.getElementById('root-meta'));
  }());
  </script>
</body>
</html>`
}
