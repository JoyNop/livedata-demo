import * as Fs from 'fs'
import * as Path from 'path'
import * as Commander from 'commander'
import { ClientOpts } from 'redis'
import { ServerLayout } from './layout'
import { LoggerConfig } from './logger'

export interface BrowserConfig {
  readonly useProxy: boolean
  readonly staticUrl: string
  readonly staticDir: string
  readonly codeUrl: string
  readonly codeDir: string
}

export interface ServerConfig {
  readonly name: string
  readonly host: string
  readonly port: number
  readonly logger: LoggerConfig
  readonly redis: ClientOpts
  readonly browser: BrowserConfig
}

type CommandItemType = 'string' | 'number' | 'boolean'

interface CommandItem {
  flags: string
  description: string
  commandName: string
  envName: string
}

interface StringCommandItem extends CommandItem {
  type: 'string'
  default: string
}

interface NumberCommandItem extends CommandItem {
  type: 'number'
  default: number
}

interface BooleanCommandItem extends CommandItem {
  type: 'boolean'
  default: boolean
}

function getStringValue(a: StringCommandItem, b?: string): string {
  if (typeof Reflect.get(Commander, a.commandName) === 'string') {
    return Reflect.get(Commander, a.commandName)
  } else if (typeof Reflect.get(process.env, a.envName) === 'string') {
    return Reflect.get(process.env, a.envName)
  } else if (typeof b === 'string') {
    return b
  } else {
    return a.default
  }
}

function getNumberValue(a: NumberCommandItem, b?: number): number {
  if (typeof Reflect.get(Commander, a.commandName) === 'string') {
    return parseInt(Reflect.get(Commander, a.commandName))
  } else if (typeof Reflect.get(process.env, a.envName) === 'string') {
    return parseInt(Reflect.get(process.env, a.envName))
  } else if (typeof b === 'number') {
    return b
  } else {
    return a.default
  }
}

function getBooleanValue(a: BooleanCommandItem, b?: number): boolean {
  if (typeof Reflect.get(Commander, a.commandName) === 'boolean') {
    return Reflect.get(Commander, a.commandName)
  } else if (typeof Reflect.get(process.env, a.envName) === 'boolean') {
    return Reflect.get(process.env, a.envName)
  } else if (typeof b === 'boolean') {
    return b
  } else {
    return a.default
  }
}

interface CommandItemMap {
  [key: string]: CommandItem | CommandItemMap
}

const DEFINITION: CommandItemMap = {
  config: {
    flags: '-f, --config-file [type]',
    description: '配置文件路径',
    commandName: 'configFile',
    envName: 'PMS_BROWSER_CONFIG_FILE',
    type: 'string',
    default: '',
  },
  name: {
    flags: '-n, --name [type]',
    description: '服务器的名字',
    commandName: 'name',
    envName: 'PMS_BROWSER_NAME',
    type: 'string',
    default: 'Pms',
  },
  host: {
    flags: '-h, --host [type]',
    description: '服务器的主机名',
    commandName: 'host',
    envName: 'PMS_BROWSER_HOST',
    type: 'string',
    default: '127.0.0.1',
  },
  port: {
    flags: '-p, --port [type]',
    description: '服务器的端口号',
    commandName: 'port',
    envName: 'PMS_BROWSER_PORT',
    type: 'number',
    default: 8080,
  },
  logger: {
    mode: {
      flags: '--logger-mode',
      description: '日志模式',
      commandName: 'loggerMode',
      envName: 'PMS_BROWSER_LOGGER_MODE',
      type: 'string',
      default: 'none',
    },
  },
  browser: {
    useProxy: {
      flags: '--browser-use-proxy',
      description: '客户端的静态文件请求路径',
      commandName: 'browserUseProxy',
      envName: 'PMS_BROWSER_BROWSER_USE_PROXY',
      type: 'boolean',
      default: true,
    },
    staticUrl: {
      flags: '--browser-static-url [type]',
      description: '客户端的静态文件请求路径',
      commandName: 'browserStaticUrl',
      envName: 'PMS_BROWSER_BROWSER_STATIC_URL',
      type: 'string',
      default: '/public',
    },
    staticDir: {
      flags: '--browser-static-dir [type]',
      description: '客户端的静态文件目录',
      commandName: 'browserStaticDir',
      envName: 'PMS_BROWSER_BROWSER_STATIC_DIR',
      type: 'string',
      default: './public',
    },
    codeUrl: {
      flags: '--browser-code-url [type]',
      description: '客户端的代码文件请求路径',
      commandName: 'browserCodeUrl',
      envName: 'PMS_BROWSER_BROWSER_CODE_URL',
      type: 'string',
      default: '/public',
    },
    codeDir: {
      flags: '--browser-code-dir [type]',
      description: '客户端的代码文件目录',
      commandName: 'browserCodeDir',
      envName: 'PMS_BROWSER_BROWSER_CODE_DIR',
      type: 'string',
      default: './public',
    },
  },
}

function parseCommandArgv() {
  let nodes: Array<CommandItemMap> = [DEFINITION]
  for (let a of nodes) {
    for (let [key, value] of Object.entries(a)) {
      if (key === 'browser' || key === 'logger') {
        nodes.push(value as CommandItemMap)
      } else if (key !== 'redis') {
        let valueCI = value as CommandItem
        Commander.option(valueCI.flags, valueCI.commandName)
      }
    }
  }
  Commander.parse(process.argv)
}

export class ServerConfig implements ServerConfig {
  public readonly name: string
  public readonly host: string
  public readonly port: number
  public readonly browser: BrowserConfig

  private static instance: ServerConfig

  public static getInstance(): ServerConfig {
    if (ServerConfig.instance === undefined) {
      ServerConfig.instance = ServerConfig.create()
    }
    return ServerConfig.instance
  }

  private static create(): ServerConfig {
    let config: any

    parseCommandArgv()

    let configFile = getStringValue(DEFINITION.config as StringCommandItem)
    if (configFile !== '') {
      config = JSON.parse(
        Fs.readFileSync(Path.join(process.cwd(), configFile), 'utf-8')
      )
    } else {
      config = Object.create(null)
    }
    if (config.logger === null || typeof config.logger !== 'object') {
      config.logger = Object.create(null)
    }
    if (config.browser === null || typeof config.browser !== 'object') {
      config.browser = Object.create(null)
    }

    config.name = getStringValue(
      DEFINITION.name as StringCommandItem,
      config.name
    )
    config.host = getStringValue(
      DEFINITION.host as StringCommandItem,
      config.host
    )
    config.port = getNumberValue(
      DEFINITION.port as NumberCommandItem,
      config.port
    )

    config.logger.mode = getStringValue(
      (DEFINITION.logger as CommandItemMap).mode as StringCommandItem,
      config.logger.mode
    )

    config.browser.useProxy = getBooleanValue(
      (DEFINITION.browser as CommandItemMap).useProxy as BooleanCommandItem,
      config.browser.useProxy
    )
    config.browser.staticUrl = getStringValue(
      (DEFINITION.browser as CommandItemMap).staticUrl as StringCommandItem,
      config.browser.staticUrl
    )
    config.browser.staticDir = getStringValue(
      (DEFINITION.browser as CommandItemMap).staticDir as StringCommandItem,
      config.browser.staticDir
    )
    config.browser.codeUrl = getStringValue(
      (DEFINITION.browser as CommandItemMap).codeUrl as StringCommandItem,
      config.browser.codeUrl
    )
    config.browser.codeDir = getStringValue(
      (DEFINITION.browser as CommandItemMap).codeDir as StringCommandItem,
      config.browser.codeDir
    )

    return config
  }
}
