import * as Redis from 'redis'
import { RedisClient, ClientOpts } from 'redis'

export class RedisManager {
  public static readonly SUB_AUTH_EXIT = 'auth/exit'

  public readonly publisher: RedisClient
  public readonly subscriber: RedisClient

  constructor(a: ClientOpts) {
    this.publisher = Redis.createClient(a)
    this.subscriber = Redis.createClient(a)
  }

  protected async onCreate() {
    await new Promise<boolean>((complete, fail) => {
      this.subscriber.subscribe(RedisManager.SUB_AUTH_EXIT, (err, reply) => {
        if (err) {
          this.publisher.end()
          this.subscriber.end()
          fail(err)
        } else {
          complete()
        }
      })
    })
  }

  protected async onDestroy() {
    this.publisher.end(true)
    this.subscriber.end(true)
  }
}
