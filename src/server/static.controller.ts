/*
  注意：这个模块提供静态文件控制器，其性能非常糟糕，每次请求都会访问磁盘（没有缓存和新鲜度校验），
  只是在开发周期提供测试使用。当部署到产品环境时，使用 NGINX 等反向代理工具
*/

import * as Fs from 'fs'
import * as Path from 'path'
import { ApplicationScope } from '@webnode/cdi'
import { ServerRequest, ServerResponse, RePathnameParams } from '@webnode/http'
import { Controller, Get, Res, Params } from '@webnode/http'
import { ServerConfig } from './config'

const CONTENT_TYPE = {
  js: 'text/javascript',
  css: 'text/css',
  map: 'application/octet-stream',
  ico: 'image/x-icon',
  png: 'image/png',
  jpg: 'image/jpeg',
  jpeg: 'image/jpeg',
  mp4: 'video/mp4',
  mpeg: 'video/mpeg',
  mov: 'video/quicktime',
  flv: 'flv-application/octet-stream',
  mp3: 'audio/x-mpeg',
  zip: 'application/zip',
  woff: 'application/x-font-woff',
  woff2: 'application/x-font-woff2',
  ttf: 'application/x-font-ttf',
  json: 'application/json',
}

function getContentType(name: string): string {
  return name in CONTENT_TYPE
    ? (CONTENT_TYPE as { [key: string]: string })[name]
    : 'text/plain'
}

@Controller
export class StaticController {
  constructor(@ApplicationScope('config') private config: ServerConfig) {}

  @Get(
    '/public/code/:basename+.(js|css|map|ico|png|jpg|jpeg|woff|woff2|ttf|json)'
  )
  private getCode(@Res res: ServerResponse, @Params params: RePathnameParams) {
    res.status = 200
    let codeDir = Path.join(__dirname, '../../', this.config.browser.codeDir)

    res.body = Fs.readFileSync(
      Path.join(codeDir, params.basename + '.' + params[0])
    )
    res.setHeader('Content-Type', getContentType(params[0]))
    res.setHeader('Content-Length', (res.body as string).length)
  }

  @Get('/public/:basename+.(js|css|map|ico|png|jpg|jpeg|woff|woff2|ttf|json)')
  private getResource(
    @Res res: ServerResponse,
    @Params params: RePathnameParams
  ) {
    res.status = 200
    let staticDir = Path.join(
      __dirname,
      '../../',
      this.config.browser.staticDir
    )

    res.body = Fs.readFileSync(
      Path.join(staticDir, params.basename + '.' + params[0])
    )
    res.setHeader('Content-Type', getContentType(params[0]))
    res.setHeader('Content-Length', (res.body as string).length)
  }
}
