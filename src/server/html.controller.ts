/*
  这个模块提供 HTML 渲染控制器，首屏访问的时候，该控制器可以提供性能优化
*/

import * as React from 'react'
import * as ReactDom from 'react-dom/server'
import * as Cookie from 'cookie'
import * as Html from './html'
import { RedisClient } from 'redis'
import { ApplicationScope } from '@webnode/cdi'
import {
  ServerRequest,
  ServerResponse,
  Controller,
  Get,
  Req,
  Res,
} from '@webnode/http'
import { ServerConfig } from './config'
import { ServerLayout } from './layout'

@Controller
export class HtmlController {
  constructor(@ApplicationScope('config') private config: ServerConfig) {}

  @Get('/:path*')
  private get(@Req req: ServerRequest, @Res res: ServerResponse) {
    res.setHeader('Content-Type', 'text/html')
    res.body = Html.render(
      ReactDom.renderToString(
        React.createElement(ServerLayout, {
          location: req.url.pathname,
        })
      ),
      this.config.browser.staticUrl,
      this.config.browser.codeUrl,
      this.config.logger.mode === 'console'
    )
  }
}
