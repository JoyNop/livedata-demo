import * as React from 'react'
import { Component, ReactNode } from 'react'
import { StaticRouter } from 'react-router-dom'
import { Layout } from '../browser/layout'

export interface ServerLayoutProps {
  location: string
}

export class ServerLayout extends Component<ServerLayoutProps> {
  public render(): ReactNode {
    return (
      <StaticRouter location={this.props.location}>
        <Layout />
      </StaticRouter>
    )
  }
}
