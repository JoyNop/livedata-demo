import axios from 'axios'
import { toast } from 'react-toastify'

//请求路径
//测试路径(本地)
const testUrl = 'http://pms.duobang.test:10001/'
//正式环境
const productUrl = 'http://pms.duodainfo.cn/'

axios.defaults.baseURL = productUrl
console.log(`Test Env:${testUrl}||Product Env:${productUrl}`)

console.warn('API环境:' + axios.defaults.baseURL)

// 请求拦截
axios.interceptors.request.use(
  config => {
    // 加载
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截  401 token过期处理
axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    // 错误提醒
    try {
      if (typeof error == 'object') {
        toast.warn(`${error.response.data.message}`, {
          position: toast.POSITION.TOP_RIGHT,
          autoClose: 1800,
        })
      }
      return Promise.reject(error)
    } catch (error) {
      console.log(error)
    }
  }
)

export default axios
