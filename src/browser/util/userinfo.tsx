import * as decode from 'jwt-decode'

interface userInfo {
  id: number
  name: string
  phone: string
  email: string
  iat: number
  exp: number
}
//获取用户默认信息
//defaultInfo().projId
//defaultInfo().subprojId
export const defaultInfo = () => {
  let defaultItem: string = localStorage.getItem('pms-default')
  let defaultInfo = JSON.parse(defaultItem)
  return defaultInfo
}

export const userInfo = () => {
  let token = localStorage.getItem('pmsToken')
  let userInfo = decode(token) as userInfo
  return userInfo
}
