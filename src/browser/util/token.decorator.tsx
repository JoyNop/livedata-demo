import * as React from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'
import * as decode from 'jwt-decode'

export function TokenDecorator(isSignIn: boolean) {
  return (target: any, name: any, descriptor: any) => {
    if (typeof window !== 'undefined') {
      const originValue = descriptor.value
      descriptor.value = function() {
        let token = localStorage.getItem('pmsToken')
        if (isSignIn) {
          if (typeof token === 'string' && checkToken(token)) {
            axios.defaults.headers.common['Authorization'] = token
            return <Redirect to="/" />
          }
          delete axios.defaults.headers.common['Authorization']
        } else {
          if (typeof token !== 'string' || !checkToken(token)) {
            delete axios.defaults.headers.common['Authorization']
            return <Redirect to="/signin" />
          }
          axios.defaults.headers.common['Authorization'] = token
        }

        return originValue.apply(this, arguments)
      }
    }

    return descriptor
  }
}

function checkToken(token: string): boolean {
  try {
    let decoded = decode(token) as any
    return decoded.exp > Date.now() / 1000
  } catch (error) {
    return false
  }
}

export default TokenDecorator
