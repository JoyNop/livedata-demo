import * as React from 'react'
import { Component, ReactNode, Fragment } from 'react'
import {
  Redirect,
  Switch,
  Route,
  StaticRouter,
  BrowserRouter,
} from 'react-router-dom'
import { SignIn } from './routes/auth/signin.com'
import { Error } from './ui/Error'
import { Style, StyleProvider } from './util/style'
import { MUI_THEME } from './ui/theme'
import ThemeProvider from '@material-ui/styles/ThemeProvider'
import { Link as MLink, NoSsr } from '@material-ui/core'
import { TodoListIndex } from './pages/todolist'
import { HomeIndex } from './pages/home'
import { BlogIndex } from './pages/blog'
import { Link } from 'react-router-dom'

export class Layout extends Component {
  public render(): ReactNode {
    return (
      <ThemeProvider theme={MUI_THEME}>
        <Switch>
          <Route path="/signin" exact component={SignIn} />
          <Route path="/" component={HomeNew} />
        </Switch>
      </ThemeProvider>
    )
  }
}

@Style({
  Container: {
    padding: '80px 100px 0 250px',
  },
  HideContainer: {
    padding: '80px 0 0 0 ',
  },
  Page: {
    width: '100%',
    minWidth: '60%',
    // 'background-color': '#fafafa'
  },
})
export class HomeNew extends Component {
  private classes = StyleProvider.of(HomeNew)
  defaultSubprojId: number
  defaultProjId: number

  public render(): ReactNode {
    let history = this.props as any

    let url: string = history.location.pathname
    let hide = true
    if (url.includes('subproj')) {
      hide = false
    }
    return (
      <div>
        <div style={{ display: 'flex', flexWrap: 'nowrap' }}>
          <Link style={{ paddingRight: '20px' }} to={'/'}>
            首页
          </Link>

          <Link style={{ paddingRight: '20px' }} to={'/todolist'}>
            todolist
          </Link>

          <Link style={{ paddingRight: '20px' }} to={'/blog'}>
            blog
          </Link>
          <Link style={{ paddingRight: '20px' }} to={'/docs/aboutlivedata.md'}>
            docs
          </Link>
          <MLink
            style={{ paddingRight: '20px' }}
            href={'https://gitlab.com/JoyNop/livedata-demo/issues'}
          >
            issues &feedback
          </MLink>
        </div>

        <Switch>
          {/* <Route path="/" exact component={DashboardRedriect} /> */}
          <Route path="/" sensitive={false} exact component={HomeIndex} />
          <Route
            path="/todolist"
            sensitive={false}
            exact
            component={TodoListIndex}
          />
          <Route path="/blog" sensitive={false} exact component={BlogIndex} />

          <Route component={Error} />
        </Switch>
      </div>
    )
  }
}

export class BrowserLayout extends Component {
  public render(): ReactNode {
    return (
      <BrowserRouter>
        <Layout />
      </BrowserRouter>
    )
  }
}
