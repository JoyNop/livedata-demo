import * as React from 'react'
import { Component, ReactNode } from 'react'
import { Helmet } from 'react-helmet'

export class About extends Component {
  public render(): ReactNode {
    return (
      <div>
        About
        <Helmet>
          <title>About Page</title>
          <meta
            name="description"
            content="This is a proof of concept for React SSR"
          />
        </Helmet>
      </div>
    )
  }
}
