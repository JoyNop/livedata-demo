import * as React from 'react'
import { Component, ReactNode, ChangeEvent } from 'react'
import { LiveData } from '@webnode/livedata'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormControl from '@material-ui/core/FormControl'

export interface UsernameLiveValue {
  username: string
}

export interface UsernameLiveData extends LiveData<UsernameLiveValue> {
  setUsername(username: string): void
}

export interface UsernameInputProps {
  name: string
  disabled?: boolean
  live: UsernameLiveData
}

export class UsernameInput extends Component<UsernameInputProps> {
  public render(): ReactNode {
    return (
      <FormControl
        fullWidth
        margin="normal"
        disabled={this.props.disabled === true ? true : false}
      >
        <InputLabel htmlFor="component-error">用户名</InputLabel>
        <Input
          id="component-error"
          aria-describedby="component-error-text"
          name={this.props.name}
          endAdornment={
            <InputAdornment position="end">
              <IconButton>
                <Icon>person</Icon>
              </IconButton>
            </InputAdornment>
          }
          onChange={(e: ChangeEvent<any>) =>
            this.props.live.setUsername(e.target.value)
          }
        />
      </FormControl>
    )
  }
}
