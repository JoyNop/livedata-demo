import { LiveData } from '@webnode/livedata'
import { UsernameLiveData, UsernameLiveValue } from './username.com'
import { PasswordInputLiveData, PasswordInputLiveValue } from './password.com'

export interface SignInValue
  extends UsernameLiveValue,
    PasswordInputLiveValue {}

export class SignInLiveData extends LiveData<SignInValue>
  implements UsernameLiveData, PasswordInputLiveData {
  private originValue: SignInValue = Object.create(null)

  public setUsername(username: string) {
    this.originValue.username = username
    this.set(this.originValue)
  }

  public setPassword(password: string) {
    this.originValue.password = password
    this.set(this.originValue)
  }
}
