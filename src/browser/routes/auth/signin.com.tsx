import * as React from 'react'
import { Component, ReactNode } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Page } from '../../ui/page'
import { Style, StyleProvider } from '../../util/style'
import { LiveComponent } from '../../util/livecom'
import { UsernameInput } from './username.com'
import { PasswordInput } from './password.com'
import { HelperText } from './helper.com'
import { SignInService, SignInServiceStatus } from './signin.service'
import { SignInLiveData } from './signin.live'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import TokenDecorator from '../../util/token.decorator'

@Style({
  box: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: '120px 0',
  },
  card: {
    padding: '40px 40px',
    backgroundColor: 'white',
    width: '400px',
    boxShadow: '0px 1px 6px rgba(0,0,0,0.06)',
    borderRadius: '3px',
    textAlign: 'center',
  },
  form: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  submit: {
    marginTop: '30px',
    marginBottom: '30px',
  },
  forgetPassword: {
    textAlign: 'center',
    color: 'rgb(153,153,153)',
    fontSize: '12px',
  },
})
export class SignIn extends LiveComponent {
  private service = new SignInService()
  private live = new SignInLiveData()

  constructor(props: {}) {
    super(props)
    this.service.observe(this, status => {
      console.log(status)

      switch (status) {
        case SignInServiceStatus.PENDING:
          this.setState({})
          break
        case SignInServiceStatus.OK:
          this.setState({})
          break
        case SignInServiceStatus.ERROR:
          this.setState({})
          break
      }
    })
  }

  @TokenDecorator(true)
  public render(): ReactNode {
    if (this.service.isAuthed()) {
      return <Redirect to="/" />
    }
    return (
      <Page color="gray">
        <div className={StyleProvider.of(SignIn).box}>
          <div className={StyleProvider.of(SignIn).card}>
            {/* <img src="https://cast-inc.monday.com/assets/logos/monday_logo_short.png" width="320px" /> */}
            <Typography variant="h5" gutterBottom>
              {' '}
              基建2025数字化云平台
            </Typography>
            <form
              className={StyleProvider.of(SignIn).form}
              onSubmit={(e: React.MouseEvent<any>) => {
                e.preventDefault()
                this.service.submit(
                  this.live.value.username,
                  this.live.value.password
                )
              }}
            >
              <UsernameInput
                name="username"
                disabled={this.service.value === SignInServiceStatus.PENDING}
                live={this.live}
              />
              <PasswordInput
                name="password"
                disabled={this.service.value === SignInServiceStatus.PENDING}
                live={this.live}
              />
              {this.service.value === SignInServiceStatus.ERROR ? (
                <HelperText message={this.service.getErrorMessage()} />
              ) : (
                false
              )}
              <Button
                className={StyleProvider.of(SignIn).submit}
                fullWidth
                variant="contained"
                color="primary"
                type="submit"
                disabled={this.service.value === SignInServiceStatus.PENDING}
              >
                登录
              </Button>
              <a className={StyleProvider.of(SignIn).forgetPassword}>
                忘记密码？
              </a>
            </form>
          </div>
        </div>
      </Page>
    )
  }
}
