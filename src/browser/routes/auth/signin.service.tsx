import { LiveData } from '@webnode/livedata'
import Axios from 'axios'
export enum SignInServiceStatus {
  NONE,
  PENDING,
  OK,
  ERROR,
}
import * as decode from 'jwt-decode'

export class SignInService extends LiveData<SignInServiceStatus> {
  private errorMessage: string
  private authed: boolean = false

  public async submit(username: string, password: string) {
    if (this.value !== SignInServiceStatus.PENDING) {
      let res: any
      this.set(SignInServiceStatus.PENDING)
      try {
        res = await Axios.post('/api/account/auth/signin', {
          username: username,
          password: password,
        })

        let token = res.data
        localStorage.setItem('pmsToken', token)
        let userInfo = decode(token) as any

        Axios.get(`/api/project/user/${userInfo.id}/home/project`).then(res => {
          let userDefault = JSON.stringify(res.data)
          localStorage.setItem('pms-default', userDefault)
        })
        this.authed = true
        this.set(SignInServiceStatus.OK)
      } catch (e) {
        if (e.response.status === 400) {
          this.errorMessage = '错误的用户名或密码'
        } else {
          this.errorMessage = '内部服务器错误'
        }
        this.set(SignInServiceStatus.ERROR)
      }
    }
  }

  public isAuthed(): boolean {
    return this.authed
  }

  public getErrorMessage(): string {
    return this.errorMessage
  }
}
