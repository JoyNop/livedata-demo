import * as React from 'react'
import { Component, ReactNode } from 'react'
import { HelperTextIcon } from '../../ui/icon'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'

export interface HelperTextProps {
  message: string
}

export class HelperText extends Component<HelperTextProps> {
  public render(): ReactNode {
    return (
      <FormControl fullWidth margin="normal" error>
        <FormHelperText id="component-error-text">
          <HelperTextIcon>error</HelperTextIcon>
          {this.props.message}
        </FormHelperText>
      </FormControl>
    )
  }
}
