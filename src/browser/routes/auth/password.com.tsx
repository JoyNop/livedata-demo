import * as React from 'react'
import { Component, ReactNode, ChangeEvent } from 'react'
import { LiveData } from '@webnode/livedata'
import Icon from '@material-ui/core/Icon'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import FormControl from '@material-ui/core/FormControl'

export interface PasswordInputLiveValue {
  password: string
}

export interface PasswordInputLiveData
  extends LiveData<PasswordInputLiveValue> {
  setPassword(password: string): void
}

export interface PasswordInputProps {
  name: string
  disabled?: boolean
  live: PasswordInputLiveData
}

export class PasswordInput extends Component<PasswordInputProps> {
  private visible = false

  private onVisibilityChange = () => {
    this.visible = !this.visible
    this.setState({})
  }

  public render(): ReactNode {
    return (
      <FormControl
        fullWidth
        margin="normal"
        disabled={this.props.disabled === true ? true : false}
      >
        <InputLabel>密码</InputLabel>
        <Input
          type={this.visible ? 'text' : 'password'}
          name={this.props.name}
          endAdornment={
            <InputAdornment position="end">
              <IconButton onClick={this.onVisibilityChange}>
                <Icon>{this.visible ? 'visibility_off' : 'visibility'}</Icon>
              </IconButton>
            </InputAdornment>
          }
          onChange={(e: ChangeEvent<any>) =>
            this.props.live.setPassword(e.target.value)
          }
        />
      </FormControl>
    )
  }
}
