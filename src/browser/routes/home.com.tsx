import * as React from 'react'
import { Component, ReactNode } from 'react'
import { Helmet } from 'react-helmet'
import { LiveData } from '@webnode/livedata'
import { Page } from '../ui/page'

export class Home extends Component {
  private liveFold: LiveData<void> = new LiveData()
  private liveLocation: LiveData<string> = new LiveData()

  public render(): ReactNode {
    return (
      // <Page color="gray">
      <div>
        Home
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="This is a proof of concept for React SSR"
          />
        </Helmet>
      </div>
    )
  }
}
