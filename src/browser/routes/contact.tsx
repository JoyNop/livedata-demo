import * as React from 'react'
import { Component, ReactNode } from 'react'
import { Helmet } from 'react-helmet'

export class Contact extends Component {
  public render(): ReactNode {
    return (
      <div>
        Contact
        <Helmet>
          <title>Contact Page</title>
          <meta
            name="description"
            content="This is a proof of concept for React SSR"
          />
        </Helmet>
      </div>
    )
  }
}
