import { LiveData } from '@webnode/livedata'
import * as React from 'react'
import { LiveComponent } from '../util/livecom'

export interface IAprops {
  live: LiveData<any>
}

export class TodoService extends LiveComponent<IAprops> {
  private todoList: Array<any> = []

   
  private originValue = Object.create(null)

  public jianyitiao(index: number) {
    this.todoList = this.props.live.value.todoList as Array<any>
    this.todoList.splice(index, 1)
    console.log(this.todoList);
    this.originValue.todoList=this.todoList
    this.props.live.set(this.originValue)
  }
}
