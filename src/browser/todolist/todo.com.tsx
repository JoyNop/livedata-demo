import * as React from 'react'
import { TodoCreate } from './todo-create.com'
import { TodoList } from './todo-list.com'
import { LiveComponent } from '../util/livecom'
import { TodoListLiveData } from './todo.live'
export class Todo extends LiveComponent {
  // private service = new SignInService()
  private live = new TodoListLiveData()

  constructor(props: {}) {
    super(props)
    this.live.observe(this, (a: any) => {
      console.log(a);

      this.setState({})
    })
  }

  render() {
    return (
      <div>
        <div style={{ border: '1px solid gray' }}>
          <p>本demo 供使用两种方法，没有做过多数据约束，仅为使用过程</p>
          <p>create采用live直接传入数据，delete采用service删除数据</p>
          <p>因为使用service需要new一个新的service，导致livedata存入的原始数据销毁</p>
          <p>若想在指定的原始数据中该操作，需要使用this.props.live.value的数据，然后重新set到指定位置</p>
        </div>
        <div>
          <TodoCreate live={this.live} />
          <TodoList live={this.live} />
        </div>
      </div>
    )
  }
}
