import * as React from 'react'
import { LiveData } from '@webnode/livedata'
import {TodoService  } from "./todo.service";

export interface ITodoList {
  live: LiveData<any>
}

export class TodoList extends React.Component<ITodoList> {
  private todo=new TodoService(this.props)
  private todoList: Array<any> = []
  render() {
    try {
      this.todoList = this.props.live.value.todoList as Array<any>
    } catch (error) {
      console.log(error)
    }

    console.log(this.todoList);    
    return (
      <div>
        {this.todoList.length > 0
          ? this.todoList.map((item: string,index:number) => {
            return <p>{`INDEX:${index} 内容：${item}`}<button onClick={()=>this.todo.jianyitiao(index)}> x</button></p>
            })
          : null}
      </div>
    )
  }
}
