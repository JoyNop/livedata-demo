import * as React from 'react'
import { LiveData } from '@webnode/livedata'
import {  TodoService} from "./todo.service";

export interface TodoLiveValue {
  todo: string
}

export interface TodoLiveData extends LiveData<TodoLiveValue> {
  setTodo(todo: string): void
}

export interface TodoInputProps { 
  live: TodoLiveData
}

export class TodoCreate extends React.Component<TodoInputProps> {
  private formElem: HTMLFormElement = null
  // private TodoService=new TodoService()

  render() {
    return (
      <div>
        <form ref={o => (this.formElem = o)}>

          <input name="todoInput" />
          <button onClick={this.addTodo}>增加</button>
        </form>

      </div>
    )
  }

  private addTodo = (event:React.ChangeEvent<any>) => {
    event.preventDefault()
    // event.stopPropagation()
    let formData = new FormData(this.formElem)
    let value = formData.get('todoInput') as string
    console.log(value);
    // this.TodoService.pushTodo(value)
 
    this.props.live.setTodo(value)
  }
}
