import { LiveData } from '@webnode/livedata'
import { TodoLiveData, TodoLiveValue } from './todo-create.com'
// import { PasswordInputLiveData, PasswordInputLiveValue } from './password.com'

export interface TodoInValue
  extends TodoLiveValue  {}

export class TodoListLiveData extends LiveData<TodoInValue>
  implements TodoLiveData {
  private originValue = Object.create(null)
  private todoList: Array<any> = []

  public setTodo(todo: string) {
    this.todoList.push(todo)
    this.originValue.todoList = this.todoList
    this.set(this.originValue)
  }
 
}
