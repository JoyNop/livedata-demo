import * as  React  from 'react'
import BlogList from "./blog-list.com";
import { LiveComponent } from '../util/livecom'

export class BlogCom extends  LiveComponent {
  render() {
    return (
      <div>
        <BlogList/>
      </div>
    )
  }
}
