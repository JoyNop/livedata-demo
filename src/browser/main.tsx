import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { BrowserLayout } from './layout'

function main() {
  let metaElem = document.getElementById('root-meta')

  if (metaElem && metaElem.getAttribute('data-debug') === 'true') {
    ReactDOM.render(<BrowserLayout />, document.getElementById('root'))
  } else {
    ReactDOM.hydrate(<BrowserLayout />, document.getElementById('root'))
  }
}

main()
