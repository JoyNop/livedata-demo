/*
  这个模块重写 MUI 的默认主题风格，在每个 ``AppComponent`` 的根节点使用 ``ThemeProvider``
  配置 ``theme``
*/

import * as Colors from '@material-ui/core/colors'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'

export const MUI_THEME = createMuiTheme({
  palette: {
    primary: Colors.blue,
  },

  overrides: {
    MuiFormLabel: {
      root: {
        focused: {
          '&$focused': {
            color: '#0095ef',
          },

          '&$error': {
            color: '#f44336',
          },
        },
      },
    },

    MuiInput: {
      underline: {
        '&:after': {
          borderBottomColor: '#0095ef',
        },
        '&:before': {
          borderBottomColor: 'rgba(0,0,0,0.12)',
        },
      },
    },

    MuiSelect: {
      root: {
        fontSize: '14px',
      },
    },

    MuiCircularProgress: {
      colorPrimary: {
        color: '#0095ef',
        // backgroundColor: 'white'
      },
    },

    MuiBackdrop: {
      root: {
        backgroundColor: 'rgba(0,0,0,0.3)',
      },
    },

    MuiButton: {
      containedPrimary: {
        paddding: '0',
        color: 'rgba(255,255,255,0.9)',
        // fontSize: '1rem',
        // boxShadow: 'none',
        // borderRadius: '2px'
      },
      textSecondary: {
        color: 'rgba(0,0,0,0.3)',
        fontSize: '1rem',
      },
    },

    MuiTabs: {
      indicator: {
        backgroundColor: '#0095ef',
      },
    },

    MuiIcon: {
      // root: {
      //   fontSize: '24px'
      // },
      colorSecondary: {
        color: '#faae59',
      },
      fontSizeLarge: {
        fontSize: '7rem',
      },
      fontSizeInherit: {
        fontSize: '2rem',
      },
    },

    MuiMenuItem: {
      root: {
        '&$selected': {
          backgroundColor: 'rgba(0, 0, 0, 0.06)!important',
        },
      },
    },
  },
})
