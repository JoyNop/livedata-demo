import * as React from 'react'
import { Component, ReactNode, Fragment } from 'react'
import { Style, StyleProvider } from '../util/style'
import Icon from '@material-ui/core/Icon'

@Style({
  root: {
    fontSize: '1rem',
    verticalAlign: 'middle',
    marginRight: '8px',
  },
})
export class HelperTextIcon extends Component {
  public render(): ReactNode {
    let classNames: Array<string> = [StyleProvider.of(HelperTextIcon).root]
    // switch (this.props.position) {
    // case 'absolute':
    //   classNames.push(this.props.classes.rootAbsolte)
    //   break
    // case 'fixed':
    // default:
    //   classNames.push(this.props.classes.rootFixed)
    //   break
    // }
    return (
      <Fragment>
        <Icon className={StyleProvider.of(HelperTextIcon).root}>
          {this.props.children}
        </Icon>
      </Fragment>
    )
  }
}
