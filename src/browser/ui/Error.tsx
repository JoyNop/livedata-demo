import * as React from 'react'
import { Style, StyleProvider } from '../../browser/util/style'
interface IError {
  errorMsg?: string //错误信息
  errorImg?: true | false | null //是否需要错误图片,需要,则为默认图片
  errorImgUrl?: string //错误图片自定义,url
  errorCom?: any //插入自定义组件
}

@Style({
  Container: {
    width: '100%',
    textAlign: 'center',
    fontSize: '10px',
  },
})
export class Error extends React.Component<IError> {
  //404NOTFOUND
  private classes = StyleProvider.of(Error)

  render() {
    return (
      <div className={this.classes.Container}>
       not found
      </div>
    )
  }
}

 
