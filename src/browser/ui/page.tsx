import * as React from 'react'
import { Component, ReactNode } from 'react'
import { Style, StyleProvider } from '../util/style'

export type PagePosition = 'absolute' | 'fixed'
export type PageColor = 'white' | 'gray'

export interface PageProps {
  position?: PagePosition
  color?: PageColor
  children: ReactNode
}

@Style({
  root: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  rootAbsolte: {
    position: 'absolute',
  },
  rootFixed: {
    position: 'fixed',
  },
  rootWhite: {
    backgroundColor: '#ffffff',
  },
  rootGray: {
    backgroundColor: '#f6f6f6',
  },
})
export class Page extends Component<PageProps> {
  public render(): ReactNode {
    let classNames: Array<string> = [StyleProvider.of(Page).root]
    switch (this.props.position) {
      case 'absolute':
        classNames.push(StyleProvider.of(Page).rootAbsolte)
        break
      case 'fixed':
      default:
        classNames.push(StyleProvider.of(Page).rootFixed)
        break
    }
    switch (this.props.color) {
      case 'gray':
        classNames.push(StyleProvider.of(Page).rootGray)
      case 'white':
      default:
        classNames.push(StyleProvider.of(Page).rootWhite)
        break
        break
    }
    return <div className={classNames.join(' ')}>{this.props.children}</div>
  }
}
